#!/bin/bash

pause_uuid="$(curl --silent -X POST -d timeout_minutes=1 http://localhost:31418/pause)"
cat locks_state.txt; echo
curl -X POST -d uuid="$pause_uuid" http://localhost:31418/unpause; echo
sleep 0.01
cat locks_state.txt; echo

echo

pause_uuid="$(curl --silent -X POST -d "" http://localhost:31418/pause)"
cat locks_state.txt; echo
curl -X POST -d uuid="$pause_uuid" http://localhost:31418/unpause; echo
sleep 0.01
cat locks_state.txt; echo

echo

curl -X POST -d timeout_minutes=1 http://localhost:31418/pause
