use chrono::{DateTime, Utc};
use log::error;
use tokio::process::Command;

pub fn run_later(
    when: DateTime<Utc>,
    what: impl std::future::Future<Output = ()> + Send + 'static,
) {
    let sleep_time = match (when - Utc::now()).to_std() {
        Ok(sleep_time) => sleep_time,
        Err(e) => {
            error!("run_later: could not convert sleep time {:?}", e);
            return;
        }
    };
    tokio::spawn(async move {
        tokio::time::sleep(sleep_time).await;
        what.await;
    });
}

fn exec(command: &'static str) {
    log::info!("executing {}", command);
    tokio::spawn(async move {
        if let Err(e) = Command::new(command).status().await {
            error!("failed to run {}: {:?}", command, e);
        }
    });
}

pub fn exec_pause() {
    exec("./pause");
}

pub fn exec_unpause() {
    exec("./unpause");
}
