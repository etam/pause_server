/*
API:
POST /pause
  DATA: timeout
  - return lock id
  - for localhost connection timeout might be not specified, it's then not saved
POST /unpause
  DATA: lock id
*/

mod locks;
mod tokio_utils;

use actix::{Actor, Addr};
use actix_web::{dev, post, web, App, HttpServer};
use chrono::Duration;
use clap::Parser;
use log::debug;
use serde::Deserialize;
use uuid::Uuid;

use crate::locks::{LocksStore, Pause, Unpause};

#[derive(Deserialize, Debug)]
struct PauseFormData {
    timeout_minutes: Option<i64>,
}

#[post("/pause")]
async fn pause(
    form: web::Form<PauseFormData>,
    locks_store: web::Data<Addr<LocksStore>>,
    conn: dev::ConnectionInfo,
) -> actix_web::Result<String> {
    debug!("got request /pause, form: {:?}", *form);
    match form.timeout_minutes {
        Some(timeout_minutes) => {
            if timeout_minutes <= 0 {
                return Err(actix_web::error::ErrorBadRequest("timeout <= 0"));
            }
        }
        None => {
            if conn.peer_addr().unwrap() != "127.0.0.1" {
                return Err(actix_web::error::ErrorBadRequest("Unspecified timeout"));
            }
        }
    }
    let uuid = locks_store
        .send(Pause(form.timeout_minutes.map(Duration::minutes)))
        .await
        .map_err(actix_web::error::ErrorInternalServerError)?
        .0;
    Ok(format!("{}", uuid))
}

#[derive(Deserialize, Debug)]
struct UnpauseFormData {
    uuid: Uuid,
}

#[post("/unpause")]
async fn unpause(
    form: web::Form<UnpauseFormData>,
    locks_store: web::Data<Addr<LocksStore>>,
) -> actix_web::Result<&'static str> {
    debug!("got request /unpause, form: {:?}", *form);
    locks_store
        .send(Unpause(form.uuid))
        .await
        .map_err(actix_web::error::ErrorInternalServerError)?
        .map_err(actix_web::error::ErrorBadRequest)?;
    Ok("Ok")
}

#[derive(Parser)]
struct Args {
    #[arg(long, default_value = "0.0.0.0")]
    bind_addr: String,
    #[arg(long, default_value_t = 31418)]
    port: u16,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenvy::dotenv().ok();
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();
    let args = Args::parse();

    let locks_store = LocksStore::start_default();

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(locks_store.clone()))
            .service(pause)
            .service(unpause)
    })
    .bind((args.bind_addr, args.port))?
    .run()
    .await
}
