use actix::{
    dev::{MessageResponse, OneshotSender},
    Actor, AsyncContext, Context, Handler, Message,
};
use atomicwrites::AtomicFile;
use chrono::{DateTime, Duration, Utc};
use log::{error, info};
use multi_index_map::MultiIndexMap;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use uuid::Uuid;

use crate::tokio_utils::{exec_pause, exec_unpause, run_later};

const STATE_FILENAME: &str = "locks_state.txt";

#[derive(MultiIndexMap, Copy, Clone, Serialize, Deserialize)]
struct Lock {
    #[multi_index(ordered_non_unique)]
    ts: DateTime<Utc>,
    #[multi_index(hashed_unique)]
    uuid: Uuid,
}

#[derive(Default)]
pub struct LocksStore {
    locks: MultiIndexLockMap,
    just_uuid: HashSet<Uuid>,
}

impl LocksStore {
    fn delete_expired_timeouts(&mut self) {
        let now = Utc::now();
        let timestamps_to_remove = self
            .locks
            .iter_by_ts()
            .filter_map(|lock| if lock.ts <= now { Some(lock.ts) } else { None })
            .collect::<Vec<_>>();
        for timestamp_to_remove in timestamps_to_remove {
            self.locks.remove_by_ts(&timestamp_to_remove);
        }
    }

    // https://github.com/lun3x/multi_index_map/issues/15
    fn schedule_next_check(&mut self, ctx: &mut Context<Self>) {
        if let Some(lock) = self.locks.iter_by_ts().next() {
            run_later(lock.ts, {
                let myself = ctx.address();
                async move {
                    let _ = myself.send(CheckTimeouts {}).await;
                }
            });
        }
    }

    fn save_to_file(&self) {
        AtomicFile::new(STATE_FILENAME, atomicwrites::AllowOverwrite)
            .write(|f| {
                serde_json::to_writer_pretty(
                    f,
                    &self
                        .locks
                        .iter()
                        .map(|(_idx, lock)| *lock)
                        .collect::<Vec<Lock>>(),
                )
            })
            .map_err(|e| {
                error!("failed to save state file: {:?}", e);
            })
            .unwrap();
    }

    fn load_from_file(&mut self) {
        let file = match std::fs::File::open(STATE_FILENAME) {
            Ok(file) => file,
            Err(e) => {
                match e.kind() {
                    std::io::ErrorKind::NotFound => {
                        // it's ok
                        return;
                    }
                    _ => {
                        error!("could not read state file: {:?}", e);
                        return;
                    }
                }
            }
        };
        let reader = std::io::BufReader::new(file);
        let locks: Vec<Lock> = serde_json::from_reader(reader)
            .map_err(|e| {
                error!("failed to deserialize state file: {:?}", e);
            })
            .unwrap();
        for lock in locks {
            self.locks.insert(lock);
        }
    }
}

impl Actor for LocksStore {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        self.load_from_file();
        self.delete_expired_timeouts();
        if self.locks.is_empty() {
            exec_unpause();
        } else {
            exec_pause();
        }
        self.schedule_next_check(ctx);
    }
}

pub struct UuidResponse(pub Uuid);

#[derive(Message)]
#[rtype(result = "UuidResponse")]
pub struct Pause(pub Option<Duration>);

#[derive(Message)]
#[rtype(result = "anyhow::Result<()>")]
pub struct Unpause(pub Uuid);

#[derive(Message)]
#[rtype(result = "()")]
struct CheckTimeouts {}

impl<A, M> MessageResponse<A, M> for UuidResponse
where
    A: Actor,
    M: Message<Result = UuidResponse>,
{
    fn handle(self, _ctx: &mut A::Context, tx: Option<OneshotSender<M::Result>>) {
        if let Some(tx) = tx {
            let _ = tx.send(self);
        }
    }
}

impl Handler<Pause> for LocksStore {
    type Result = UuidResponse;

    fn handle(&mut self, msg: Pause, ctx: &mut Context<Self>) -> Self::Result {
        let uuid = Uuid::new_v4();

        match msg.0 {
            Some(timeout) => {
                let ts = Utc::now() + timeout;
                info!("new lock: {}, timeout at {}", uuid, ts.format("%FT%TZ"));
                self.locks.insert(Lock { ts, uuid });
                run_later(ts, {
                    let myself = ctx.address();
                    async move {
                        let _ = myself.send(CheckTimeouts {}).await;
                    }
                });
                self.save_to_file();
            }
            None => {
                info!("new lock: {}, without timeout", uuid);
                self.just_uuid.insert(uuid);
            }
        }

        exec_pause();

        UuidResponse(uuid)
    }
}

impl Handler<Unpause> for LocksStore {
    type Result = anyhow::Result<()>;

    fn handle(&mut self, msg: Unpause, ctx: &mut Context<Self>) -> Self::Result {
        info!("unpausing {}", msg.0);
        let was_with_timeout: bool = self.locks.remove_by_uuid(&msg.0).is_some();
        let was_without_timeout: bool = self.just_uuid.remove(&msg.0);
        if !was_with_timeout && !was_without_timeout {
            anyhow::bail!("unknown timeout");
        }
        tokio::spawn(ctx.address().send(CheckTimeouts {}));
        Ok(())
    }
}

impl Handler<CheckTimeouts> for LocksStore {
    type Result = ();

    fn handle(&mut self, _msg: CheckTimeouts, ctx: &mut Context<Self>) -> Self::Result {
        info!("checking timeouts");

        self.delete_expired_timeouts();
        self.schedule_next_check(ctx);
        self.save_to_file();
        if self.locks.is_empty() && self.just_uuid.is_empty() {
            exec_unpause();
        }
    }
}
